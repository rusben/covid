$(document).ready(function(){
  console.log("Welcome to jQuery.");


// 1 - #alan
// 1 - #alan
const C4 = new Audio("sounds/C4.mp3");
const D4 = new Audio("sounds/D4.mp3");
const E4 = new Audio("sounds/E4.mp3");
const F4 = new Audio("sounds/F4.mp3");
const G4 = new Audio("sounds/G4.mp3");
const A4 = new Audio("sounds/A4.mp3");
const B4 = new Audio("sounds/B4.mp3");
const C5 = new Audio("sounds/C5.mp3");
const D5 = new Audio("sounds/D5.mp3");
const E5 = new Audio("sounds/E5.mp3");

const playSound = audio => {
  const clone = audio.cloneNode();
  clone.play();
  setTimeout(() => (clone.volume = 0.8), 400);
  setTimeout(() => (clone.volume = 0.6), 800);
  setTimeout(() => (clone.volume = 0.4), 1200);
  setTimeout(() => (clone.volume = 0.2), 1600);
  setTimeout(() => (clone.volume = 0), 2000);
};

// C4
const C4Key = document.querySelector(".C4-key");
const playC4 = () => {
  playSound(C4);
  C4Key.classList.add("active");
  setTimeout(() => C4Key.classList.remove("active"), 200);
};
C4Key.addEventListener("click", playC4);


// D4
const D4Key = document.querySelector(".D4-key");
const playD4 = () => {
  playSound(D4);
  D4Key.classList.add("active");
  setTimeout(() => D4Key.classList.remove("active"), 200);
};
D4Key.addEventListener("click", playD4);


// E4
const E4Key = document.querySelector(".E4-key");
const playE4 = () => {
  playSound(E4);
  E4Key.classList.add("active");
  setTimeout(() => E4Key.classList.remove("active"), 200);
};
E4Key.addEventListener("click", playE4);

// F4
const F4Key = document.querySelector(".F4-key");
const playF4 = () => {
  playSound(F4);
  F4Key.classList.add("active");
  setTimeout(() => F4Key.classList.remove("active"), 200);
};
F4Key.addEventListener("click", playF4);


// G4
const G4Key = document.querySelector(".G4-key");
const playG4 = () => {
  playSound(G4);
  G4Key.classList.add("active");
  setTimeout(() => G4Key.classList.remove("active"), 200);
};
G4Key.addEventListener("click", playG4);


// A4
const A4Key = document.querySelector(".A4-key");
const playA4 = () => {
  playSound(A4);
  A4Key.classList.add("active");
  setTimeout(() => A4Key.classList.remove("active"), 200);
};
A4Key.addEventListener("click", playA4);

// B4
const B4Key = document.querySelector(".B4-key");
const playB4 = () => {
  playSound(B4);
  B4Key.classList.add("active");
  setTimeout(() => B4Key.classList.remove("active"), 200);
};
B4Key.addEventListener("click", playB4);

// C5
const C5Key = document.querySelector(".C5-key");
const playC5 = () => {
  playSound(C5);
  C5Key.classList.add("active");
  setTimeout(() => C5Key.classList.remove("active"), 200);
};
C5Key.addEventListener("click", playC5);

// D5
const D5Key = document.querySelector(".D5-key");
const playD5 = () => {
  playSound(D5);
  D5Key.classList.add("active");
  setTimeout(() => D5Key.classList.remove("active"), 200);
};
D5Key.addEventListener("click", playD5);



// E5
const E5Key = document.querySelector(".E5-key");
const playE5 = () => {
  playSound(E5);
  E5Key.classList.add("active");
  setTimeout(() => E5Key.classList.remove("active"), 200);
};
E5Key.addEventListener("click", playE5);


  $("#Interruptor").click(function(){
    $(".alan-cell").toggleClass("main");
    $(".slider").toggleClass("main2");
  });

// 2 - #brian

// 3 - #chema

// 4 - #mary
$("#mary span").css({"font-family": "'Arvo'"});
var percent = 0
$(".progress").css({"background-color" : "#74d04c", "border-radius" : "5px" });
$("#mark").css({"border-radius" : "5px", "background-color" : "#e6e6e6"});
$("#pregunta1").css({"display" : "none"});
$("#finish").css({"display" : "none"});
$(".pruebas").css({"border-radius" : "5px", "background-color" : "white", "cursor" : "pointer"});
$("#play").css({"background-color" : "#99ccff", "border-radius" : "5px", "color" : "white"});

$("#play").click(function() {
  $("#play").hide();
  $("#pregunta1").show("slow");
});

$("#limpiar").click(function(){
 $("#input1").val('');
});

$(".pruebas").click(function(){
  var textBoto = $(this).text();
  var textAntic = $("#input1").val();
  $("#input1").val(textAntic + textBoto);

  $("#verification2").click(function(){
  if ( textAntic + textBoto == ' pon  más  jabón '){
    alert("¡Correcto!")
    percent +=100;
    $(".progress").css("width", percent+'%')
    $(".progress").attr('aria-valuenow', percent);
    $("#pregunta1").hide("slow");
    $("#finish").show("slow");
  }
});
});

// 5 - #masana
$("#masana span").css({"color" : "red"});

// 6 - #pol

// 7 - #joel

// 8 - #ruben
var inicio = true;

$(document).ready(function() {


	$("#ruben-boton").click(function(){
		if(inicio){
			$("#ruben-cuadrado").css({"background-color":"#000000", "border-color":"#2980b9"});
			inicio = false;
			$("#ruben-cuadrado2").css({"background-color":"#FFFFFF", "border-color":"#FF622C"});
			inicio = false;
    }
		else{
			$("#ruben-cuadrado").css({"background-color":"#FFFFFF", "border-color":"#FF622C"});
			inicio = true;
      $("#ruben-cuadrado2").css({"background-color":"#000000", "border-color":"#2980b9"});
			inicio = true;
		}
	});
});


// 9 - #samu
$(".button-samu").on("click", function() {

  $( ".cell" ).css({
    "background-color" : getRandomColor()
  });



function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }


});

// 10 - #tarik
  $("#flip").click(function(){
    $("#panel").slideDown(5000);
  });
  $("#stop").click(function(){
    $("#panel").stop();
  });
// 11 - #xiao



$("#xiao").css({"display" : "grid" , "grid-template-columns" : "repeat(2, 1fr)" , "grid-auto-rows" : "minmax(auto, auto)" });


$("#mainSquare").css({"margin" : "10px" , "padding" : "5px" , "width" : "100px" , "height" : "100px" , "background-color" : "black" , "opacity" : "0.5" , "display" : "grid" , "grid-template-columns" : "repeat(3, 1fr)" , "grid-auto-rows" : "minmax(10px, auto)" }).click(function(){
  $('#mainSquare').css({
     'width': '250px',
     'height': '250px',
 });

$("#mainSquare2").css({"display" : "none"});

 $('.items').css({
    'color': 'red',
    'width': '40px',
    'height': '40px',
    'margin': '20px',
 });
});

$(".items").css({"background-color" : "red" , "color" : "white" , "width" : "20px" , "height" : "20px" , "margin " : "10px"})

$("#mainSquare2").css({"margin" : "10px" , "padding" : "5px" , "width" : "100px" , "height" : "100px" , "background-color" : "black" , "opacity" : "0.5" , "display" : "grid" , "grid-template-columns" : "repeat(3, 1fr)" , "grid-auto-rows" : "minmax(10px, auto)" }).click(function(){
  $('#mainSquare2').css({
     'width': '250px',
     'height': '250px',
 });

 $('.items2').css({
    'color': 'red',
    'width': '40px',
    'height': '40px',
    'margin': '20px',
 });

 $("#mainSquare").css({"display" : "none"});
});

$(".items2").css({"background-color" : "blue" , "color" : "white" , "width" : "20px" , "height" : "20px" , "margin " : "10px"});





});
